# The script fetches the actual currency rates in relation to CZK from cnb.cz website

import requests
r = requests.get('https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt')
rates = r.text.strip().split('\n')

# skip the original header
rates = rates[2:]

# create own header
print("%20s %15s %6s %6s %20s\n" % ("Country","Currency","Amount","Code","Rate"))

for rate in rates:
    rate = rate.split('|')
    print("%20s %15s %6s %6s %20s" % tuple(rate))

